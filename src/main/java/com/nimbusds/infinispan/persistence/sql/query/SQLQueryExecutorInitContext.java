package com.nimbusds.infinispan.persistence.sql.query;


import javax.sql.DataSource;

import com.nimbusds.infinispan.persistence.sql.SQLRecordTransformer;
import org.jooq.SQLDialect;


/**
 * SQL query executor initialisation context.
 */
public interface SQLQueryExecutorInitContext<K,V> {
	
	
	/**
	 * Returns the configured SQL data source.
	 *
	 * @return The SQL data source.
	 */
	DataSource getDataSource();
	
	
	/**
	 * Returns the configured SQL record transformer.
	 *
	 * @return The SQL record transformer.
	 */
	SQLRecordTransformer<K,V> getSQLRecordTransformer();
	
	
	/**
	 * Returns the configured SQL dialect.
	 *
	 * @return The SQL dialect.
	 */
	SQLDialect getSQLDialect();
}
