package com.nimbusds.infinispan.persistence.sql;


import java.util.LinkedList;
import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.Table;


/**
 * SQL table utilities.
 */
public class SQLTableUtils {
	
	
	/**
	 * Gets the column names of the specified table.
	 *
	 * @param table      The table.
	 * @param dslContext The DSL context.
	 *
	 * @return The column names, empty list if none.
	 */
	public static List<String> getColumnNames(final Table<?> table, final DSLContext dslContext) {
		
		Result result = dslContext.selectFrom(table).limit(0).fetch();
		
		List<String> columnNames = new LinkedList<>();
		
		for (int i=0; i < result.fields().length; i++)
			columnNames.add(result.fields()[i].getName());
		
		return columnNames;
	}
	
	
	private SQLTableUtils(){}
}
