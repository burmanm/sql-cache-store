package com.nimbusds.infinispan.persistence.sql;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Log4j loggers.
 */
public final class Loggers {


	/**
	 * Main logger. Records general configuration, startup, shutdown and
	 * system messages.
	 */
	public static final Logger MAIN_LOG = LogManager.getLogger("MAIN");


	/**
	 * SQL database request / response logger.
	 */
	public static final Logger SQL_LOG = LogManager.getLogger("SQL");
}
