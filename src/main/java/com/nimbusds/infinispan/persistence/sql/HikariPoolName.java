package com.nimbusds.infinispan.persistence.sql;


import java.util.Properties;

import net.jcip.annotations.Immutable;


/**
 * Hikari connection pool name.
 */
@Immutable
public final class HikariPoolName {
	
	
	/**
	 * The connection pool name.
	 */
	private final String poolName;
	
	
	/**
	 * Creates a new Hikari connection pool name.
	 *
	 * @param poolName The name. Must not be {@code null}.
	 */
	private HikariPoolName(final String poolName) {
		assert poolName != null;
		this.poolName = poolName;
	}
	
	
	@Override
	public String toString() {
		return poolName;
	}
	
	
	/**
	 * Sets a default poolName property as "cacheName.sqlStore".
	 *
	 * @param props     The Hikari properties. Must not be {@code null}.
	 * @param cacheName The configured Infinispan cache name. Must not be
	 *                  {@code null}.
	 *
	 * @return The set pool name.
	 */
	public static HikariPoolName setDefaultPoolName(final Properties props, final String cacheName) {
		
		if (props.containsKey("poolName")) {
			return new HikariPoolName(props.getProperty("poolName"));
		}
		
		HikariPoolName poolName = new HikariPoolName(cacheName + ".sqlStore");
		
		props.setProperty("poolName", poolName.toString());
		
		return poolName;
	}
}
