package com.nimbusds.infinispan.persistence.sql;


import com.codahale.metrics.Timer;
import net.jcip.annotations.ThreadSafe;

import com.nimbusds.common.monitor.MonitorRegistries;


/**
 * SQL operations timers.
 */
@ThreadSafe
class SQLTimers {


	/**
	 * Times load operations.
	 */
	final Timer loadTimer = new Timer();


	/**
	 * Times write operations.
	 */
	final Timer writeTimer = new Timer();


	/**
	 * Times delete operations.
	 */
	final Timer deleteTimer = new Timer();


	/**
	 * Times item process operations.
	 */
	final Timer processTimer = new Timer();


	/**
	 * Times purge expired Infinispan entries operations.
	 */
	final Timer purgeTimer = new Timer();


	/**
	 * Creates a new set of SQL operations timers and puts them into
	 * the singleton {@link MonitorRegistries}.
	 *
	 * @param prefix The prefix for the timer names. Must not be
	 *               {@code null}.
	 */
	SQLTimers(final String prefix) {
		MonitorRegistries.register(prefix + "sqlStore.loadTimer", loadTimer);
		MonitorRegistries.register(prefix + "sqlStore.writeTimer", writeTimer);
		MonitorRegistries.register(prefix + "sqlStore.deleteTimer", deleteTimer);
		MonitorRegistries.register(prefix + "sqlStore.processTimer", processTimer);
		MonitorRegistries.register(prefix + "sqlStore.purgeTimer", purgeTimer);
	}
}
