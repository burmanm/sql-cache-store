package com.nimbusds.infinispan.persistence.sql.config;


import java.util.Properties;

import net.jcip.annotations.Immutable;
import org.apache.commons.lang3.StringUtils;
import org.infinispan.commons.configuration.BuiltBy;
import org.infinispan.commons.configuration.ConfigurationFor;
import org.infinispan.commons.configuration.attributes.Attribute;
import org.infinispan.commons.configuration.attributes.AttributeDefinition;
import org.infinispan.commons.configuration.attributes.AttributeSet;
import org.infinispan.commons.util.StringPropertyReplacer;
import org.infinispan.configuration.cache.AbstractStoreConfiguration;
import org.infinispan.configuration.cache.AsyncStoreConfiguration;
import org.infinispan.configuration.cache.SingletonStoreConfiguration;
import org.jooq.SQLDialect;

import com.nimbusds.common.config.LoggableConfiguration;
import com.nimbusds.infinispan.persistence.sql.Loggers;
import com.nimbusds.infinispan.persistence.sql.SQLRecordTransformer;
import com.nimbusds.infinispan.persistence.sql.SQLStore;


/**
 * SQL store configuration.
 * 
 * <p>Example XML configuration:
 * 
 * <pre>
 * &lt;infinispan xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 * 	          xsi:schemaLocation="urn:infinispan:config:8.2 http://www.infinispan.org/schemas/infinispan-config-8.2.xsd"
 * 	          xmlns="urn:infinispan:config:8.2"
 * 	          xmlns:sql="urn:infinispan:config:store:sql:2.2"&gt;
 * 	
 * 	&lt;cache-container name="myCacheContainer" default-cache="myMap" statistics="true"&gt;
 * 		&lt;jmx duplicate-domains="true"/&gt;
 * 		&lt;local-cache name="myMap"&gt;
 * 			&lt;eviction type="COUNT" size="100"/&gt;
 * 			&lt;persistence passivation="false"&gt;
 * 				&lt;sql-store xmlns="urn:infinispan:config:store:sql:2.2"
 * 			      		      shared="true"
 * 			      		      record-transformer="com.nimbusds.infinispan.persistence.sql.UserRecordTransformer"
 * 			      		      sql-dialect="H2"
 * 			      		      create-table-if-missing="true"&gt;
 *
 * 				&lt;property name="jdbcUrl"&gt;jdbc:h2:mem:test;DATABASE_TO_UPPER=false&lt;/property&gt;
 * 				&lt;property name="username"&gt;admin&lt;/property&gt;
 * 				&lt;property name="password"&gt;secret&lt;/property&gt;
 *
 * 				&lt;/sql-store&gt;
 * 			&lt;/persistence&gt;
 * 		&lt;/local-cache&gt;
 * 	&lt;/cache-container&gt;
 * 	
 * &lt;/infinispan&gt;
 * </pre>
 */
@Immutable
@BuiltBy(SQLStoreConfigurationBuilder.class)
@ConfigurationFor(SQLStore.class)
public class SQLStoreConfiguration extends AbstractStoreConfiguration implements LoggableConfiguration {
	
	
	/**
	 * The attribute definition for the record transformer class.
	 */
	static final AttributeDefinition<Class> RECORD_TRANSFORMER = AttributeDefinition.builder("recordTransformer", null, Class.class).build();
	
	
	/**
	 * The attribute definition for the query executor class.
	 */
	static final AttributeDefinition<Class> QUERY_EXECUTOR = AttributeDefinition.builder("queryExecutor", null, Class.class).build();
	
	
	/**
	 * The attribute definition for the SQL dialect.
	 */
	static final AttributeDefinition<SQLDialect> SQL_DIALECT = AttributeDefinition.builder("sqlDialect", SQLDialect.DEFAULT).build();
	
	
	/**
	 * The attribute definition for the optional create table if missing
	 * setting.
	 */
	static final AttributeDefinition<Boolean> CREATE_TABLE_IF_MISSING = AttributeDefinition.builder("createTableIfMissing", Boolean.TRUE).build();
	
	
	/**
	 * The attribute definition for the optional connection pool reference.
	 */
	static final AttributeDefinition<String> CONNECTION_POOL = AttributeDefinition.builder("connectionPool", null, String.class).build();
	
	
	/**
	 * Returns the attribute definitions for the SQL store configuration.
	 *
	 * @return The attribute definitions.
	 */
	public static AttributeSet attributeDefinitionSet() {
		return new AttributeSet(SQLStoreConfiguration.class,
			AbstractStoreConfiguration.attributeDefinitionSet(),
			RECORD_TRANSFORMER, QUERY_EXECUTOR, SQL_DIALECT, CREATE_TABLE_IF_MISSING, CONNECTION_POOL);
	}
	
	
	/**
	 * The class for transforming between Infinispan entries (key / value
	 * pair and optional metadata) and a corresponding SQL record.
	 *
	 * <p>See {@link SQLRecordTransformer}.
	 */
	private final Attribute<Class> recordTransformerClass;
	
	
	/**
	 * The optional class for executing direct SQL queries against the
	 * database.
	 *
	 * <p>See {@link com.nimbusds.infinispan.persistence.common.query.QueryExecutor}
	 */
	private final Attribute<Class> queryExecutorClass;
	
	
	/**
	 * The configured SQL dialect.
	 */
	private final Attribute<SQLDialect> sqlDialect;
	
	
	/**
	 * The configured optional create table if missing setting.
	 */
	private final Attribute<Boolean> createTableIfMissing;
	
	
	/**
	 * The configured connection pool reference.
	 */
	private final Attribute<String> connectionPool;


	/**
	 * Creates a new SQL store configuration.
	 *
	 * @param attributes           The configuration attributes. Must not be
	 *                             {@code null}.
	 * @param asyncConfig          Configuration for the async cache
	 *                             loader.
	 * @param singletonStoreConfig Configuration for a singleton store.
	 */
	public SQLStoreConfiguration(final AttributeSet attributes,
				     final AsyncStoreConfiguration asyncConfig,
				     final SingletonStoreConfiguration singletonStoreConfig) {

		super(attributes, asyncConfig, singletonStoreConfig);
		
		recordTransformerClass = attributes.attribute(RECORD_TRANSFORMER);
		assert recordTransformerClass != null;
		
		queryExecutorClass = attributes.attribute(QUERY_EXECUTOR);
		
		sqlDialect = attributes.attribute(SQL_DIALECT);
		assert sqlDialect != null;
		
		createTableIfMissing = attributes.attribute(CREATE_TABLE_IF_MISSING);
		
		connectionPool = attributes.attribute(CONNECTION_POOL);
	}
	
	
	/**
	 * Returns the class for transforming between Infinispan entries (key /
	 * value pairs and optional metadata) and a corresponding SQL record.
	 *
	 * <p>See {@link SQLRecordTransformer}.
	 *
	 * @return The record transformer class.
	 */
	public Class getRecordTransformerClass() {
		
		return recordTransformerClass.get();
	}
	
	
	/**
	 * Returns the optional class for executing direct SQL queries against
	 * the database.
	 *
	 * <p>See {@link com.nimbusds.infinispan.persistence.common.query.QueryExecutor}
	 *
	 * @return The query executor class, {@code null} if not specified.
	 */
	public Class getQueryExecutorClass() {
		
		return queryExecutorClass.get();
	}
	
	
	/**
	 * Returns the configured SQL dialect.
	 *
	 * @return The SQL dialect.
	 */
	public SQLDialect getSQLDialect() {
		
		return sqlDialect.get();
	}
	
	
	/**
	 * Returns the configured create table if missing setting.
	 *
	 * @return {@code true} to create the underlying table(s) if missing,
	 *         {@code false} to skip this check.
	 */
	public boolean createTableIfMissing() {
		
		return createTableIfMissing.get();
	}
	
	
	/**
	 * Returns the configured connection pool reference.
	 *
	 * @return The connection pool reference, {@code null} if none.
	 */
	public String getConnectionPool() {
		
		return connectionPool.get();
	}
	
	
	@Override
	public Properties properties() {
		
		// Interpolate with system properties where ${sysPropName} is found
		
		Properties interpolatedProps = new Properties();
		
		for (String name: super.properties().stringPropertyNames()) {
			interpolatedProps.setProperty(name, StringPropertyReplacer.replaceProperties(super.properties().getProperty(name)));
		}
		
		return interpolatedProps;
	}
	
	
	@Override
	public void log() {
		
		Loggers.MAIN_LOG.info("[IS0000] Infinispan SQL store: Record transformer class: {} ", getRecordTransformerClass().getCanonicalName());
		Loggers.MAIN_LOG.info("[IS0001] Infinispan SQL store: Query executor class: {} ", getQueryExecutorClass() != null ? getQueryExecutorClass().getCanonicalName() : "not specified");
		Loggers.MAIN_LOG.info("[IS0002] Infinispan SQL store: SQL dialect: {} ", sqlDialect);
		Loggers.MAIN_LOG.info("[IS0004] Infinispan SQL store: Create table if missing: {} ", createTableIfMissing);
		Loggers.MAIN_LOG.info("[IS0008] Infinispan SQL store: Connection pool reference: {} ", getConnectionPool() != null ? getConnectionPool() : "not specified");
		
		if (StringUtils.isNotBlank(properties().getProperty("dataSourceClassName"))) {
			Loggers.MAIN_LOG.info("[IS0005] Infinispan SQL store: Data source class name: {} ", properties().getProperty("dataSourceClassName"));
		}
		
		if (StringUtils.isNotBlank(properties().getProperty("dataSource.url"))) {
			Loggers.MAIN_LOG.info("[IS0006] Infinispan SQL store: Data source URL: {} ", properties().getProperty("dataSource.url"));
		}
		
		// Old style JDBC URL config
		if (StringUtils.isNotBlank(properties().getProperty("jdbcUrl"))) {
			Loggers.MAIN_LOG.info("[IS0003] Infinispan SQL store: JDBC URL: {} ", properties().getProperty("jdbcUrl"));
		}
	}
}
