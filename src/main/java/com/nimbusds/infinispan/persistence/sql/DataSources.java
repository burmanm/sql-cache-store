package com.nimbusds.infinispan.persistence.sql;


import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.zaxxer.hikari.HikariDataSource;
import net.jcip.annotations.ThreadSafe;


/**
 * Shareable HikariCP data sources.
 */
@ThreadSafe
class DataSources {
	
	
	private final Map<String, HikariDataSource> dataSources = new ConcurrentHashMap<>();
	
	
	private final  Map<String, List<SQLStore<?,?>>> awaitingStartup = new ConcurrentHashMap<>();
	
	
	/**
	 * Puts a data source for the specified cache name.
	 *
	 * @param cacheName  The cache name. Must not be {@code null}.
	 * @param dataSource The data source. Must not be {@code null}.
	 */
	synchronized void put(final String cacheName, final HikariDataSource dataSource) {
		
		dataSources.put(cacheName, dataSource);
		
		List<SQLStore<?,?>> storedAwaitingStartup = awaitingStartup.remove(cacheName);
		if (storedAwaitingStartup == null) {
			Loggers.MAIN_LOG.info("[IS0160] SQL store: No caches found awaiting the connection pool start for {}", cacheName);
			return;
		}
		
		for (SQLStore<?,?> toStartUp: storedAwaitingStartup) {
			Loggers.MAIN_LOG.info("[IS0161] SQL store: Resuming start for {} using the connection pool of {}", toStartUp.getCacheName(), cacheName);
			toStartUp.start();
		}
	}
	
	
	/**
	 * Gets the data source for the specified cache name.
	 *
	 * @param cacheName The cache name. Must not be {@code null}.
	 *
	 * @return The data source, {@code null} if not specified.
	 */
	HikariDataSource get(final String cacheName) {
		
		return dataSources.get(cacheName);
	}
	
	
	/**
	 * Defers the start of the specified SQL store.
	 *
	 * @param cacheName The name of the cache which must become available.
	 *                  Must not be {@code null}.
	 * @param sqlStore  The SQL store to defer its start. Must not be
	 *                  {@code null}.
	 */
	synchronized void deferStart(final String cacheName, final SQLStore<?,?> sqlStore) {
		
		final List<SQLStore<?,?>> singletonList = new CopyOnWriteArrayList<>();
		singletonList.add(sqlStore);
		
		List<SQLStore<?,?>> existingList = awaitingStartup.putIfAbsent(cacheName, singletonList);
		
		if (existingList != null) {
			existingList.add(sqlStore);
			Loggers.MAIN_LOG.info("[IS0138] SQL store: Deferred start of store using a shared connection pool: {}", sqlStore.getCacheName());
		} else {
			Loggers.MAIN_LOG.info("[IS0139] SQL store: Deferred start of store using a shared connection pool: {}", sqlStore.getCacheName());
		}
	}
	
	
	/**
	 * Removes a data source.
	 *
	 * @param cacheName The cache name for the data source. Must not be
	 *                  {@code null}.
	 */
	void remove(final String cacheName) {
		
		dataSources.remove(cacheName);
	}
}
