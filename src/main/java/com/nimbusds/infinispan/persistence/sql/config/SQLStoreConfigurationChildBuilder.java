package com.nimbusds.infinispan.persistence.sql.config;


import org.infinispan.configuration.cache.StoreConfigurationChildBuilder;
import org.jooq.SQLDialect;


/**
 * SQL store configuration child builder.
 */
public interface SQLStoreConfigurationChildBuilder<S> extends StoreConfigurationChildBuilder<S> {
	
	
	/**
	 * Sets the class for transforming between Infinispan entries (key /
	 * value pair and optional metadata) and a corresponding SQL record.
	 *
	 * @param recordTransformerClass The record transformer class. Must not
	 *                               be {@code null}.
	 *
	 * @return The builder.
	 */
	SQLStoreConfigurationBuilder recordTransformerClass(final Class recordTransformerClass);
	
	
	/**
	 * Sets the optional class for executing direct SQL queries against the
	 * database.
	 *
	 * @param queryExecutorClass The query executor class, {@code null} if
	 *                           not specified.
	 *
	 * @return The builder.
	 */
	SQLStoreConfigurationBuilder queryExecutorClass(final Class queryExecutorClass);
	
	
	/**
	 * Sets the preferred SQL dialect.
	 *
	 * @param sqlDialect The preferred SQL dialect. Must not be
	 *                   {@code null}.
	 *
	 * @return The builder.
	 */
	SQLStoreConfigurationBuilder sqlDialect(final SQLDialect sqlDialect);
	
	
	/**
	 * Sets the optional create table if missing configuration.
	 *
	 * @param createTableIfMissing {@code true} to create the underlying
	 *                             SQL table(s) if they are missing (the
	 *                             default setting), {@code false} to skip
	 *                             this check.
	 *
	 * @return The builder.
	 */
	SQLStoreConfigurationBuilder createTableIfMissing(final boolean createTableIfMissing);
	
	
	/**
	 * Sets the optional connection pool reference.
	 *
	 * @param cacheName The cache name for which to use its SQL store
	 *                  connection pool, {@code null} if not specified.
	 *
	 * @return The builder.
	 */
	SQLStoreConfigurationBuilder connectionPool(final String cacheName);
}
