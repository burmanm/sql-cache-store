package com.nimbusds.infinispan.persistence.sql;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.jooq.impl.DSL.table;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import net.jcip.annotations.ThreadSafe;
import org.infinispan.marshall.core.MarshalledEntry;
import org.infinispan.marshall.core.MarshalledEntryFactory;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.AdvancedCacheWriter;
import org.jooq.DSLContext;


/**
 * Expired entry reaper.
 */
@ThreadSafe
class ExpiredEntryReaper<K,V> {
	
	
	/**
	 * The Infinispan marshalled entry factory.
	 */
	private final MarshalledEntryFactory<K, V> marshalledEntryFactory;
	
	
	/**
	 * The DSL context.
	 */
	private final DSLContext dsl;


	/**
	 * The SQL record transformer.
	 */
	private final SQLRecordTransformer<K,V> recordTransformer;


	/**
	 * Creates a new reaper for expired entries.
	 *
	 * @param marshalledEntryFactory The Infinispan marshalled entry
	 *                               factory. Must not be {@code null}.
	 * @param dsl                    The DSL context. Must not be
	 *                               {@code null}.
	 * @param recordTransformer      The SQL record transformer. Must not
	 *                               be {@code null}.
	 */
	public ExpiredEntryReaper(final MarshalledEntryFactory<K,V> marshalledEntryFactory,
				  final DSLContext dsl,
				  final SQLRecordTransformer<K,V> recordTransformer) {

		assert marshalledEntryFactory != null;
		this.marshalledEntryFactory = marshalledEntryFactory;
		
		assert dsl != null;
		this.dsl = dsl;
		
		assert recordTransformer != null;
		this.recordTransformer = recordTransformer;
	}


	/**
	 * Purges the expired persisted entries according to their metadata
	 * timestamps (if set / persisted).
	 *
	 * @param purgeListener The purge listener. Must not be {@code null}.
	 */
	public void purge(final AdvancedCacheWriter.PurgeListener<? super K> purgeListener) {

		final long now = new Date().getTime();

		// The keys for deletion
		List<K> forDeletion = new LinkedList<>();
		
		dsl.select()
			.from(table(recordTransformer.getTableName()))
			.stream()
			.forEach(record -> {
				
				InfinispanEntry<K,V> infinispanEntry = recordTransformer.toInfinispanEntry(record);
				
				InternalMetadata metadata = infinispanEntry.getMetadata();
				
				if (metadata == null) {
					return; // no metadata found
				}
				
				if (metadata.isExpired(now)) {
					
					// Mark record for deletion
					forDeletion.add(infinispanEntry.getKey());
				}
			});

		int counter = 0;
		
		for (K key: forDeletion) {

			// Delete SQL record
			int numDeleted = dsl.deleteFrom(table(recordTransformer.getTableName()))
				.where(recordTransformer.resolveSelectionConditions(key))
				.execute();
			
			if (numDeleted == 1) {
				// Notify listener, interested in the Infinispan entry key
				purgeListener.entryPurged(key);
				counter++;
			}
		}
		
		Loggers.SQL_LOG.debug("[IS0128] SQL store: Purged {} expired {} cache entries", counter, recordTransformer.getTableName());
	}
	
	
	/**
	 * Purges the expired persisted entries according to their metadata
	 * timestamps (if set / persisted), with an extended listener for the
	 * complete purged entry.
	 *
	 * @param purgeListener The purge listener. Must not be {@code null}.
	 */
	public void purgeExtended(final AdvancedCacheExpirationWriter.ExpirationPurgeListener<K,V> purgeListener) {

		final long now = new Date().getTime();

		// The entries for deletion
		List<InfinispanEntry<K,V>> forDeletion = new LinkedList<>();
		
		dsl.select()
			.from(table(recordTransformer.getTableName()))
			.stream()
			.forEach(record -> {
				
				InfinispanEntry<K,V> infinispanEntry = recordTransformer.toInfinispanEntry(record);
				
				InternalMetadata metadata = infinispanEntry.getMetadata();
				
				if (metadata == null) {
					return; // no metadata found
				}
				
				if (metadata.isExpired(now)) {
					
					// Mark record for deletion
					forDeletion.add(infinispanEntry);
				}
			});
		
		
		int counter = 0;
		
		for (InfinispanEntry<K,V> en: forDeletion) {

			// Delete SQL record
			int numDeleted = dsl.deleteFrom(table(recordTransformer.getTableName()))
				.where(recordTransformer.resolveSelectionConditions(en.getKey()))
				.execute();
			
			if (numDeleted == 1) {
				// Notify listener, interested in the Infinispan entry
				MarshalledEntry<K,V> marshalledEntry =
					marshalledEntryFactory.newMarshalledEntry(
						en.getKey(),
						en.getValue(),
						en.getMetadata()
					);
				purgeListener.marshalledEntryPurged(marshalledEntry);
				counter++;
			}
		}
		
		Loggers.SQL_LOG.debug("[IS0128] SQL store: Purged {} expired {} cache entries", counter, recordTransformer.getTableName());
	}
}
