package com.nimbusds.infinispan.persistence.sql.config;


import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;

import static org.infinispan.commons.util.StringPropertyReplacer.replaceProperties;

import org.infinispan.commons.CacheConfigurationException;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.cache.PersistenceConfigurationBuilder;
import org.infinispan.configuration.parsing.*;
import org.jooq.SQLDialect;
import org.kohsuke.MetaInfServices;


/**
 * XML configuration parser for XML schema v2.7
 */
@MetaInfServices
@Namespaces({
	@Namespace(uri = "urn:infinispan:config:store:sql:2.7", root = "sql-store"),
	@Namespace(root = "sql-store")
})
public class SQLStoreConfigurationParser27 implements ConfigurationParser {
	
	
	@Override
	public void readElement(final XMLExtendedStreamReader reader, final ConfigurationBuilderHolder holder)
		throws XMLStreamException {
		ConfigurationBuilder builder = holder.getCurrentConfigurationBuilder();
		
		Element element = Element.forName(reader.getLocalName());
		switch (element) {
			case SQL_STORE: {
				this.parseSQLStore(reader, builder.persistence(), holder.getClassLoader());
				break;
			}
			default: {
				throw ParseUtils.unexpectedElement(reader);
			}
		}
	}
	
	
	/**
	 * Parses an SQL store XML element.
	 */
	private void parseSQLStore(final XMLExtendedStreamReader reader,
				   final PersistenceConfigurationBuilder persistenceBuilder,
				   final ClassLoader classLoader)
		throws XMLStreamException {
		
		SQLStoreConfigurationBuilder builder = new SQLStoreConfigurationBuilder(persistenceBuilder);
		
		this.parseSQLStoreAttributes(reader, builder, classLoader);
		
		while (reader.hasNext() && (reader.nextTag() != XMLStreamConstants.END_ELEMENT)) {
			Element element = Element.forName(reader.getLocalName());
			switch (element) {
				default: {
					Parser.parseStoreElement(reader, builder);
					break;
				}
			}
		}
		
		persistenceBuilder.addStore(builder);
	}
	
	
	/**
	 * Parses the attributes of an XML store element.
	 */
	private void parseSQLStoreAttributes(final XMLExtendedStreamReader reader,
					     final SQLStoreConfigurationBuilder builder,
					     final ClassLoader classLoader)
		throws XMLStreamException {
		
		for (int i = 0; i < reader.getAttributeCount(); i++) {
			ParseUtils.requireNoNamespaceAttribute(reader, i);
			String value = replaceProperties(reader.getAttributeValue(i));
			Attribute attribute = Attribute.forName(reader.getAttributeLocalName(i));
			switch (attribute) {
				case RECORD_TRANSFORMER: {
					try {
						builder.recordTransformerClass(classLoader.loadClass(value));
					} catch (ClassNotFoundException e) {
						throw new CacheConfigurationException(e);
					}
					break;
				}
				
				case QUERY_EXECUTOR: {
					try {
						builder.queryExecutorClass(classLoader.loadClass(value));
					} catch (ClassNotFoundException e) {
						throw new CacheConfigurationException(e);
					}
					break;
				}
				
				case SQL_DIALECT: {
					builder.sqlDialect(SQLDialect.valueOf(value.toUpperCase()));
					break;
				}
				
				case CREATE_TABLE_IF_MISSING: {
					if ("true".equals(value)) {
						builder.createTableIfMissing(true);
					} else if ("false".equals(value)){
						builder.createTableIfMissing(false);
					} else {
						throw new CacheConfigurationException("Invalid " + attribute.getLocalName() + ": Must be true or false");
					}
					break;
				}
				
				case CONNECTION_POOL: {
					if (value != null && ! value.trim().isEmpty()) {
						builder.connectionPool(value);
					}
					break;
				}
				
				default: {
					Parser.parseStoreAttribute(reader, i, builder);
					break;
				}
			}
		}
	}
	
	
	@Override
	public Namespace[] getNamespaces() {
		return ParseUtils.getNamespaceAnnotations(getClass());
	}
}
