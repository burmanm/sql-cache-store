/**
 * Transformation utilities for Java collections and other complex classes.
 */
package com.nimbusds.infinispan.persistence.sql.transformers;