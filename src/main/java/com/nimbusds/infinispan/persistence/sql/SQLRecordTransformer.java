package com.nimbusds.infinispan.persistence.sql;


import java.util.Collection;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.sql.config.SQLStoreConfiguration;
import org.jooq.Condition;
import org.jooq.Record;
import org.jooq.SQLDialect;


/**
 * Interface for transforming between Infinispan entries (key / value pair and
 * metadata) and a corresponding SQL record. Implementations must be
 * thread-safe.
 *
 * <p>To specify an entry transformer for a given Infinispan cache that is
 * backed by an SQL store, provide its class name to the
 * {@link SQLStoreConfiguration store configuration}.
 */
public interface SQLRecordTransformer<K,V> {
	
	
	/**
	 * SQL transformer initialisation parameters.
	 */
	interface InitParameters {
		
		
		/**
		 * Returns the configured SQL dialect.
		 *
		 * @return The SQL dialect.
		 */
		SQLDialect sqlDialect();
	}
	
	
	/**
	 * Initialises the SQL transformer.
	 *
	 * @param initParams The initialisation parameters.
	 */
	void init(final InitParameters initParams);
	
	
	/**
	 * Returns the SQL statement to create a table for the entries.
	 * Typically required at SQL store startup if the underlying table
	 * doesn't exist yet. The statement must be aware of the SQL dialect of
	 * the {@link #init configured} database.
	 *
	 * @return The SQL create table statement.
	 */
	String getCreateTableStatement();
	
	
	/**
	 * Returns the associated SQL table name.
	 *
	 * @return The table name.
	 */
	String getTableName();
	
	
	/**
	 * Resolves the SQL selection conditions for the specified Infinispan
	 * entry key.
	 *
	 * @param key The Infinispan entry key. Not {@code null}.
	 *
	 * @return One or more WHERE conditions.
	 */
	Collection<Condition> resolveSelectionConditions(final K key);
	

	/**
	 * Transforms the specified Infinispan entry (key / value pair with
	 * optional metadata) to an SQL record ready to be written.
	 *
	 * <p>Example:
	 *
	 * <p>Infinispan entry:
	 *
	 * <ul>
	 *     <li>Key: cae7t
	 *     <li>Value: Java POJO with fields {@code uid=cae7t},
	 *         {@code givenName=Alice}, {@code surname=Adams} and
	 *         {@code email=alice@wonderland.net}.
	 *     <li>Metadata: Specifies the entry expiration and other
	 *         properties.
	 * </ul>
	 *
	 * <p>Resulting SQL record:
	 *
	 * <pre>
	 * uid: cae7t (key)
	 * surname: Adams
	 * given_name: Alice
	 * email: alice@wonderland.net
	 * </pre>
	 *
	 * @param infinispanEntry The Infinispan entry. Not {@code null}.
	 *
	 * @return The SQL record.
	 */
	SQLRecord toSQLRecord(final InfinispanEntry<K,V> infinispanEntry);


	/**
	 * Transforms the specified SQL record to an Infinispan entry (key /
	 * value / metadata triple).
	 *
	 * <p>Example:
	 *
	 * <p>SQL record:
	 *
	 * <pre>
	 * uid: cae7t
	 * surname: Adams
	 * given_name: Alice
	 * email: alice@wonderland.net
	 * </pre>
	 *
	 * <p>Resulting Infinispan entry:
	 *
	 * <ul>
	 *     <li>Key: cae7t
	 *     <li>Value: Java POJO with fields {@code uid=cae7t},
	 *         {@code givenName=Alice}, {@code surname=Adams} and
	 *         {@code email=alice@wonderland.net}.
	 *     <li>Metadata: Default metadata (no expiration, etc).
	 * </ul>
	 *
	 * @param sqlRecord The SQL record. Must not be {@code null}.
	 *
	 * @return The Infinispan entry (key / value pair with optional
	 *         metadata).
	 */
	InfinispanEntry<K,V> toInfinispanEntry(final Record sqlRecord);
}
