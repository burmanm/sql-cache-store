package com.nimbusds.infinispan.persistence.sql;


import java.util.List;


/**
 * Interface for transforming an SQL table. Intended to facilitate table add,
 * modify and drop column changes.
 */
public interface SQLTableTransformer {
	
	
	/**
	 * Returns a list of SQL table transform statements.
	 *
	 * @param columnNames The current table column names.
	 *
	 * @return The list of SQL table transform statements to execute,
	 *         {@code null} if none.
	 */
	List<String> getTransformTableStatements(final List<String> columnNames);
}
