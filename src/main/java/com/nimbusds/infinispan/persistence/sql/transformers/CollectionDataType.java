package com.nimbusds.infinispan.persistence.sql.transformers;


/**
 * SQL data type for storing Java collections.
 */
public enum CollectionDataType {
	
	
	/**
	 * SQL array.
	 */
	ARRAY,
	
	
	/**
	 * JSON array.
	 */
	JSON
}
