package com.nimbusds.infinispan.persistence.sql;


import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;


public class HikariConfigUtilsTest {
	

	@Test
	public void testRemoveNonHikariProps() {
		
		Properties properties = new Properties();
		properties.setProperty("dataSourceClassName", "com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
		properties.setProperty("dataSource.url", "jdbc:mysql://localhost/database");
		properties.setProperty("dataSource.user", "test");
		properties.setProperty("dataSource.password", "test");
		properties.setProperty("dataSource.cachePrepStmts", "true");
		properties.setProperty("dataSource.prepStmtCacheSize", "250");
		properties.setProperty("dataSource.prepStmtCacheSqlLimit", "2048");
		properties.setProperty("sqlStore.sqlDialect", "MySQL");
		int sizeIn = properties.size();
		
		Properties out = HikariConfigUtils.removeNonHikariProperties(properties);
		
		assertEquals(sizeIn - 1, out.size());
		
		assertFalse(out.containsKey("sqlStore.sqlDialect"));
	}
	
	
	@Test
	public void testSetDefaultPoolName() {
		
		Properties properties = new Properties();
		HikariPoolName poolName = HikariPoolName.setDefaultPoolName(properties, "sessionStore.sessionMap");
		assertEquals("sessionStore.sessionMap.sqlStore", poolName.toString());
		assertEquals("sessionStore.sessionMap.sqlStore", properties.get("poolName"));
		assertEquals(1, properties.size());
	}
	
	
	@Test
	public void testOverrideDefaultPoolName() {
		
		Properties properties = new Properties();
		properties.setProperty("poolName", "myPoolName");
		HikariPoolName poolName = HikariPoolName.setDefaultPoolName(properties, "sessionStore.sessionMap");
		assertEquals("myPoolName", poolName.toString());
		assertEquals("myPoolName", properties.get("poolName"));
		assertEquals(1, properties.size());
	}
}
