package com.nimbusds.infinispan.persistence.sql;


import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

import org.jooq.Field;
import org.jooq.SQLDialect;
import org.jooq.conf.RenderNameStyle;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;


/**
 * Verifies the output SQL query from JOOQ for H2, MySQL, PostgreSQL.
 */
public class SQLDialectsTest extends TestWithSQLDatabase {
	
	
	private static final Settings H2_SETTINGS;
	
	static {
		H2_SETTINGS = new Settings();
		H2_SETTINGS.setRenderNameStyle(RenderNameStyle.AS_IS);
	}
	
	
	public void testContains() {
		
		String expectedH2 = "select 1 one\n" +
			"from users\n" +
			"where uid = '123'";
		
		assertEquals(expectedH2, DSL.using(persistentConnection, SQLDialect.H2, H2_SETTINGS)
			.selectOne()
			.from(table("users"))
			.where(field("uid").equal("123"))
			.toString());
		
		
		String expectedMySQL = "select 1 as `one`\n" +
			"from users\n" +
			"where uid = '123'";
		
		assertEquals(expectedMySQL, DSL.using(persistentConnection, SQLDialect.MYSQL)
			.selectOne()
			.from(table("users"))
			.where(field("uid").equal("123"))
			.toString());
		
		
		String expectedP95 = "select 1 as \"one\"\n" +
			"from users\n" +
			"where uid = '123'";
		
		assertEquals(expectedP95, DSL.using(persistentConnection, SQLDialect.POSTGRES_9_5)
			.selectOne()
			.from(table("users"))
			.where(field("uid").equal("123"))
			.toString());
	}
	
	
	public void testLoad() {
		
		String expected = "select *\n" +
			"from users\n" +
			"where uid = '123'";
		
		assertEquals(expected, DSL.using(persistentConnection,           SQLDialect.H2).selectFrom(table("users")).where(field("uid").equal("123")).toString());
		assertEquals(expected, DSL.using(persistentConnection,        SQLDialect.MYSQL).selectFrom(table("users")).where(field("uid").equal("123")).toString());
		assertEquals(expected, DSL.using(persistentConnection, SQLDialect.POSTGRES_9_5).selectFrom(table("users")).where(field("uid").equal("123")).toString());
	}
	
	
	public void testDeleteOne() {
		
		String expected = "delete from users\n" +
			"where uid = '123'";
		
		assertEquals(expected, DSL.using(persistentConnection,           SQLDialect.H2).deleteFrom(table("users")).where(field("uid").equal("123")).toString());
		assertEquals(expected, DSL.using(persistentConnection,        SQLDialect.MYSQL).deleteFrom(table("users")).where(field("uid").equal("123")).toString());
		assertEquals(expected, DSL.using(persistentConnection, SQLDialect.POSTGRES_9_5).deleteFrom(table("users")).where(field("uid").equal("123")).toString());
	}
	
	
	public void testUpsert() {
		
		// Different impl across DBs
		
		Collection<Field<?>> cols = Arrays.asList(field("uid"), field("name"), field("email"));
		Collection<Field<?>> keys = Collections.singleton(field("uid"));
		
		String expectedH2 = "merge into users\n" +
			"(uid, name, email) key (uid) values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com'\n" +
			")";
		
		assertEquals(expectedH2, DSL.using(persistentConnection, SQLDialect.H2, H2_SETTINGS)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com")
			.toString());
		
		
		String expectedMySQL = "insert into users (\n" +
			"  uid, \n" +
			"  name, \n" +
			"  email\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com'\n" +
			")\n" +
			"on duplicate key update\n" +
			"  uid = '123', \n" +
			"  name = 'alice', \n" +
			"  email = 'alice@email.com'";
		
		assertEquals(expectedMySQL, DSL.using(persistentConnection, SQLDialect.MYSQL)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com")
			.toString());
		
		
		String expectedP95 = "insert into users (\n" +
			"  uid, \n" +
			"  name, \n" +
			"  email\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com'\n" +
			")\n" +
			"on conflict (uid) do update\n" +
			"set uid = '123', \n" +
			"    name = 'alice', \n" +
			"    email = 'alice@email.com'";
		
		assertEquals(expectedP95, DSL.using(persistentConnection, SQLDialect.POSTGRES_9_5)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com")
			.toString());
	}
	
	
	public void testStringListColumn() {
		
		// Different impl across DBs
		
		Collection<Field<?>> cols = Arrays.asList(field("uid"), field("name"), field("email"), field("permissions"));
		Collection<Field<?>> keys = Collections.singleton(field("uid"));
		
		String expectedH2 = "merge into users\n" +
			"(uid, name, email, permissions) key (uid) values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com', \n" +
			"  '[admin, audit]'\n" +
			")";
		
		assertEquals(expectedH2, DSL.using(persistentConnection, SQLDialect.H2, H2_SETTINGS)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com", Arrays.asList("admin", "audit"))
			.toString());
		
		String expectedMySQL = "insert into users (\n" +
			"  uid, \n" +
			"  name, \n" +
			"  email, \n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com', \n" +
			"  '[admin, audit]'\n" +
			")\n" +
			"on duplicate key update\n" +
			"  uid = '123', \n" +
			"  name = 'alice', \n" +
			"  email = 'alice@email.com', \n" +
			"  permissions = '[admin, audit]'";
		
		assertEquals(expectedMySQL, DSL.using(persistentConnection, SQLDialect.MYSQL)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com", Arrays.asList("admin", "audit"))
			.toString());
		
		String expectedP95 = "insert into users (\n" +
			"  uid, \n" +
			"  name, \n" +
			"  email, \n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com', \n" +
			"  '[admin, audit]'\n" +
			")\n" +
			"on conflict (uid) do update\n" +
			"set uid = '123', \n" +
			"    name = 'alice', \n" +
			"    email = 'alice@email.com', \n" +
			"    permissions = '[admin, audit]'";
		
		assertEquals(expectedP95, DSL.using(persistentConnection, SQLDialect.POSTGRES_9_5)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com", Arrays.asList("admin", "audit"))
			.toString());
	}
	
	public void testStringArrayColumn() {
		
		// Different impl across DBs
		
		Collection<Field<?>> cols = Arrays.asList(field("uid"), field("name"), field("email"), field("permissions"));
		Collection<Field<?>> keys = Collections.singleton(field("uid"));
		
		String expectedH2 = "merge into users\n" +
			"(uid, name, email, permissions) key (uid) values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com', \n" +
			"  ('admin', 'audit')\n" +
			")";
		
		assertEquals(expectedH2, DSL.using(persistentConnection, SQLDialect.H2, H2_SETTINGS)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com", new String[]{"admin", "audit"})
			.toString());
		
		String expectedMySQL = "insert into users (\n" +
			"  uid, \n" +
			"  name, \n" +
			"  email, \n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com', \n" +
			"  array['admin', 'audit']\n" +
			")\n" +
			"on duplicate key update\n" +
			"  uid = '123', \n" +
			"  name = 'alice', \n" +
			"  email = 'alice@email.com', \n" +
			"  permissions = array['admin', 'audit']";
		
		assertEquals(expectedMySQL, DSL.using(persistentConnection, SQLDialect.MYSQL)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com", new String[]{"admin", "audit"})
			.toString());
		
		String expectedP95 = "insert into users (\n" +
			"  uid, \n" +
			"  name, \n" +
			"  email, \n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com', \n" +
			"  cast('{\"admin\",\"audit\"}' as varchar[])\n" +
			")\n" +
			"on conflict (uid) do update\n" +
			"set uid = '123', \n" +
			"    name = 'alice', \n" +
			"    email = 'alice@email.com', \n" +
			"    permissions = cast('{\"admin\",\"audit\"}' as varchar[])";
		
		assertEquals(expectedP95, DSL.using(persistentConnection, SQLDialect.POSTGRES_9_5)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com", new String[]{"admin", "audit"})
			.toString());
	}
	
	
	public void testDateTime() {
		
		Timestamp date = new Timestamp(1474006010900L);
		
		Collection<Field<?>> cols = Arrays.asList(field("uid"), field("name"), field("registered"));
		Collection<Field<?>> keys = Collections.singleton(field("uid"));
		
		String expectedH2 = "merge into users\n" +
			"(uid, name, registered) key (uid) values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  '" + toSQLTimestamp(date.toInstant()) + "'\n" +
			")";
		
		assertEquals(expectedH2, DSL.using(persistentConnection, SQLDialect.H2, H2_SETTINGS)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", date.toString())
			.toString());
		
		String expectedMySQL = "insert into users (\n" +
			"  uid, \n" +
			"  name, \n" +
			"  registered\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  '" + toSQLTimestamp(date.toInstant()) + "'\n" +
			")\n" +
			"on duplicate key update\n" +
			"  uid = '123', \n" +
			"  name = 'alice', \n" +
			"  registered = '" + toSQLTimestamp(date.toInstant()) + "'";
		
		assertEquals(expectedMySQL, DSL.using(persistentConnection, SQLDialect.MYSQL)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", date.toString())
			.toString());
		
		String expectedP95 = "insert into users (\n" +
			"  uid, \n" +
			"  name, \n" +
			"  registered\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  '" + toSQLTimestamp(date.toInstant()) + "'\n" +
			")\n" +
			"on conflict (uid) do update\n" +
			"set uid = '123', \n" +
			"    name = 'alice', \n" +
			"    registered = '" + toSQLTimestamp(date.toInstant()) + "'";
		
		assertEquals(expectedP95, DSL.using(persistentConnection, SQLDialect.POSTGRES_9_5)
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", date.toString())
			.toString());
	}
}
