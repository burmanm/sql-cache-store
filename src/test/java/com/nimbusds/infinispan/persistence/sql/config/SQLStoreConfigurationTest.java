package com.nimbusds.infinispan.persistence.sql.config;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

import org.infinispan.commons.util.StringPropertyReplacer;
import org.junit.Test;


public class SQLStoreConfigurationTest {
	

	public static Properties getTestJDBCProperties() {

		Properties props = new Properties();

		try {
			props.load(new FileInputStream("jdbc-h2.properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return props;
	}
	
	
	@Test
	public void testNestedInterpolationNotSupported() {
		
		Properties props = new Properties();
		props.setProperty("catalina.home", "/opt/tomcat/");
		
		String out = StringPropertyReplacer.replaceProperties("${dataSource.url:h2:file:${catalina.home}/c2id}", props);
		
		// Nested props clearly don't work
		assertEquals("h2:file:${catalina.home/c2id}", out);
	}
}
