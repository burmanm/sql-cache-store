package com.nimbusds.infinispan.persistence.sql;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

import com.nimbusds.infinispan.persistence.common.query.QueryExecutor;
import com.nimbusds.infinispan.persistence.common.query.SimpleMatchQuery;
import org.infinispan.Cache;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.jooq.Record;
import org.jooq.impl.DSL;


/**
 * Tests the SQL store with an XML-based config.
 */
public class SQLStoreWithXMLConfigTest extends TestWithSQLDatabase {


	private static final String USER_CACHE_NAME = "userMap";
	
	private static final String GROUP_CACHE_NAME = "groupMap";


	/**
	 * The Infinispan cache manager.
	 */
	protected EmbeddedCacheManager cacheMgr;
	
	
	private static Connection getSQLConnection()
		throws SQLException {
		
		return ((SQLStore)SQLStore.getInstances().get(USER_CACHE_NAME)).getDataSource().getConnection();
	}


	@Override
	public void setUp()
		throws Exception {

		super.setUp();

		cacheMgr = new DefaultCacheManager("test-infinispan.xml");

		cacheMgr.start();
	}


	@Override
	public void tearDown()
		throws Exception {
		
		// Shut down Infinispan first
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		// Shut down H2 DB
		super.tearDown();
	}


	public void testSimpleObjectLifeCycle()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(USER_CACHE_NAME);
		
		assertNotNull(SQLStore.getInstances().get(USER_CACHE_NAME));
		assertEquals(1, SQLStore.getInstances().size());
		
		// Initial size
		assertEquals(0, myMap.size());
		
		// Get non-existing object
		assertNull(myMap.get("invalid-key"));
		
		// Store new object
		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@wonderland.net");
		
		assertNull(myMap.putIfAbsent(k1, u1));
		
		// Check presence
		assertTrue(myMap.containsKey(k1));
		
		
		// Confirm SQL insert
		Record r = sql.select().from(table("test_users")).where(field("uid").eq(k1)).fetchOne();
		assertEquals(k1, r.get("uid"));
		assertEquals("Alice",  r.get("given_name"));
		assertEquals("Adams",  r.get("surname"));
		assertEquals("alice@wonderland.net",  r.get("email"));
		assertNull(r.get("created"));
		assertNull(r.get("permissions"));
		assertEquals(6, r.size());
		
		
		// Get new count
		assertEquals(1, myMap.size());
		
		// Get object
		assertEquals("Alice Adams", myMap.get(k1).getName());
		assertEquals("alice@wonderland.net", myMap.get(k1).getEmail());
		
		// Update object
		u1 = new User("Bob Brown", "bob@wonderland.net");
		assertNotNull(myMap.replace(k1, u1));
		
		// Confirm SQL update
		r = sql.select().from(table("test_users")).where(field("uid").eq(k1)).fetchOne();
		assertEquals(k1, r.get("uid"));
		assertEquals("Bob",  r.get("given_name"));
		assertEquals("Brown",  r.get("surname"));
		assertEquals("bob@wonderland.net",  r.get("email"));
		assertNull(r.get("created"));
		assertNull(r.get("permissions"));
		assertEquals(6, r.size());
		
		// Get object
		assertEquals("Bob Brown", myMap.get(k1).getName());
		assertEquals("bob@wonderland.net", myMap.get(k1).getEmail());
		
		// Remove object
		u1 = myMap.remove(k1);
		assertEquals("Bob Brown", u1.getName());
		assertEquals("bob@wonderland.net", u1.getEmail());
		
		// Confirm removal
		assertNull(myMap.get(k1));
		assertFalse(myMap.containsKey(k1));
		
		// Confirm SQL delete
		try (Connection c = getSQLConnection()) {
			assertNull(DSL.using(c).select().from(table("test_users")).where(field("uid").eq(k1)).fetchOne());
		}
		
		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
	}
	
	
	public void testMultiplePutIfAbsentWithEviction()
		throws Exception {
		
		Cache<String, User> myMap = cacheMgr.getCache(USER_CACHE_NAME);
		
		assertEquals(100, myMap.getCacheConfiguration().memory().size());
		assertEquals(EvictionType.COUNT, myMap.getCacheConfiguration().memory().evictionType());
		assertEquals(60000, myMap.getCacheConfiguration().expiration().wakeUpInterval());
		
		String[] keys = new String[200];
		
		for (int i=0; i < 200; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com");
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		// Confirm SQL insert
		for (int i=0; i < 200; i ++) {
			
			try (Connection c = getSQLConnection()) {
				Record r = DSL.using(c).select().from(table("test_users")).where(field("uid").eq(keys[i])).fetchOne();
				assertEquals(keys[i], r.get("uid"));
				assertEquals("Alice",  r.get("given_name"));
				assertEquals("Adams-" + i,  r.get("surname"));
				assertEquals("alice-"+i+"@email.com",  r.get("email"));
				assertNull(r.get("created"));
				assertNull(r.get("permissions"));
				assertEquals(6, r.size());
			}
		}
		
		// Calls AdvancedCacheLoader.process, result matches eviction size
		assertEquals(100, myMap.getAdvancedCache().getDataContainer().size());
		
		// Calls AdvancedCacheLoader.process, result equals total persisted
		assertEquals(200, myMap.size());
		
		// Check presence
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}
		
		assertEquals(200, myMap.size());
		
		// Evict entries from memory, LDAP store unaffected
		for (String uuid: keys) {
			myMap.evict(uuid);
		}
		
		assertEquals(200, myMap.size());
		
		// Recheck presence, loads data from LDAP
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}
		
		// Confirm SQL presence
		for (int i=0; i < 200; i ++) {
			
			Record r = sql.select().from(table("test_users")).where(field("uid").eq(keys[i])).fetchOne();
			assertEquals(keys[i], r.get("uid"));
			assertEquals("Alice",  r.get("given_name"));
			assertEquals("Adams-" + i,  r.get("surname"));
			assertEquals("alice-"+i+"@email.com",  r.get("email"));
			assertNull(r.get("created"));
			assertNull(r.get("permissions"));
			assertEquals(6, r.size());
		}
		
		// Evict entries from memory, LDAP store unaffected
		for (String uuid: keys) {
			myMap.evict(uuid);
		}
		
		assertEquals(200, myMap.size());
		
		// Iterate and count, calls AdvancedCacheLoader.process
		AtomicInteger counter = new AtomicInteger();
		myMap.keySet().iterator().forEachRemaining(uuid -> counter.incrementAndGet());
		assertEquals(200, counter.get());
		
		// Remove from cache and LDAP store
		for (int i=0; i < 200; i ++) {
			assertNotNull(myMap.remove(keys[i]));
		}
		
		assertEquals(0, myMap.size());
		
		// Confirm SQL deletion
		for (int i=0; i < 200; i ++) {
			
			assertNull(sql.select().from(table("test_users")).where(field("uid").eq(keys[i])).fetchOne());
		}
		
		// Confirm deletion
		for (int i=0; i < 200; i ++) {
			
			assertNull(myMap.get(keys[0]));
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void testDirectSQLQueryExecution() {
		
		Cache<String, User> myMap = cacheMgr.getCache(USER_CACHE_NAME);
		
		QueryExecutor<String,User> queryExecutor = SQLStore.getInstances().get(USER_CACHE_NAME).getQueryExecutor();
		
		// Expect empty result set
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> fail("Unexpected result"));
		
		assertTrue(myMap.isEmpty());
		
		
		myMap.put("a", new User("Alice Adams", "alice@wonderland.net"));
		
		
		List<User> matches = new ArrayList<>();
		
		// Expect 1 result
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> matches.add(infinispanEntry.getValue()));
		
		assertEquals("Alice Adams", matches.get(0).getName());
		assertEquals("alice@wonderland.net", matches.get(0).getEmail());
		assertEquals(1, matches.size());
		
		myMap.put("b", new User("Bob Brown", "bob@wonderland.net"));
		
		// Expect 1 result
		matches.clear();
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> matches.add(infinispanEntry.getValue()));
		
		assertEquals("Alice Adams", matches.get(0).getName());
		assertEquals("alice@wonderland.net", matches.get(0).getEmail());
		assertEquals(1, matches.size());
		
		
		myMap.put("c", new User("Claire Cox", "alice@wonderland.net"));
		
		// Expect 2 results
		matches.clear();
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> matches.add(infinispanEntry.getValue()));
		
		assertEquals("Alice Adams", matches.get(0).getName());
		assertEquals("alice@wonderland.net", matches.get(0).getEmail());
		
		assertEquals("Claire Cox", matches.get(1).getName());
		assertEquals("alice@wonderland.net", matches.get(1).getEmail());
		assertEquals(2, matches.size());
		
		myMap.remove("a");
		
		// Expect 1 result
		matches.clear();
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> matches.add(infinispanEntry.getValue()));
		
		assertEquals("Claire Cox", matches.get(0).getName());
		assertEquals("alice@wonderland.net", matches.get(0).getEmail());
		assertEquals(1, matches.size());
		
		
		myMap.clear();
		
		// Expect zero results
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> fail("Unexpected result"));
	}
	
	
	public void testSharedDisabled() {
		
		Cache<String, User> myMap = cacheMgr.getCache(USER_CACHE_NAME);
		
		assertFalse(myMap.getCacheConfiguration().persistence().stores().get(0).shared());
	}
	
	
	public void testSharedConnectionPool() {
		
		Cache<String,User> userMap = cacheMgr.getCache(USER_CACHE_NAME);
		Cache<String,String> groupMap = cacheMgr.getCache(GROUP_CACHE_NAME);
		
		userMap.get("test");
		groupMap.put("eng", "eng@example.com");
		
		assertNotNull(SQLStore.getInstances().get(USER_CACHE_NAME));
		assertNotNull(SQLStore.getInstances().get(GROUP_CACHE_NAME));
		assertEquals(2, SQLStore.getInstances().size());
	}
}