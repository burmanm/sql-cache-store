package com.nimbusds.infinispan.persistence.sql;


import java.time.Instant;
import java.util.List;
import java.util.Objects;

import net.jcip.annotations.Immutable;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * User POJO, test class.
 */
@Immutable
public final class User {
	
	private final String name;
	
	private final String email;
	
	private final Instant created;
	
	private final List<String> permissions;
	
	
	public User(final String name, final String email) {
		this(name, email, null, null);
	}
	
	
	public User(final String name, final String email, final Instant created, final List<String> permissions) {
		this.name = name;
		this.email = email;
		this.created = created;
		this.permissions = permissions;
	}
	
	
	public String getName() {
		return name;
	}
	
	
	public String getEmail() {
		return email;
	}
	
	
	public Instant getCreated() {
		return created;
	}
	
	
	public List<String> getPermissions() {
		return permissions;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;
		
		User user = (User) o;
		
		if (!Objects.equals(name, user.name))
			return false;
		if (!Objects.equals(email, user.email))
			return false;
		if (!Objects.equals(created, user.created))
			return false;
		return Objects.equals(permissions, user.permissions);
		
	}
	
	
	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (created != null ? created.hashCode() : 0);
		result = 31 * result + (permissions != null ? permissions.hashCode() : 0);
		return result;
	}
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
			.append("name", name)
			.append("email", email)
			.append("created", created)
			.append("permissions", permissions)
			.toString();
	}
}
