package com.nimbusds.infinispan.persistence.sql;


import junit.framework.TestCase;


public class LoggersTest extends TestCase{
	

	public void testNames() {

		assertEquals("MAIN", Loggers.MAIN_LOG.getName());
		assertEquals("SQL", Loggers.SQL_LOG.getName());
	}
}
