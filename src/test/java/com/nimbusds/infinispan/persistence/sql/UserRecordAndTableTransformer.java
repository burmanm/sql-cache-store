package com.nimbusds.infinispan.persistence.sql;


import java.util.Collections;
import java.util.List;

import org.jooq.SQLDialect;


public class UserRecordAndTableTransformer extends UserRecordTransformer implements SQLTableTransformer {
	
	
	@Override
	public List<String> getTransformTableStatements(List<String> columnNames) {
		
		if (columnNames.contains("address")) {
			return null; // skip
		}
		
		if (SQLDialect.H2.equals(sqlDialect)) {
			
			return Collections.singletonList("ALTER TABLE IF EXISTS test_users " +
				"ADD COLUMN IF NOT EXISTS address VARCHAR(255) NULL " +
				"AFTER email");
		}
		
		if (SQLDialect.MYSQL.equals(sqlDialect)) {
			
			return Collections.singletonList("ALTER TABLE test_users " +
				"ADD COLUMN address VARCHAR(255) NULL " +
				"AFTER email");
		}
		
		if (SQLDialect.POSTGRES.equals(sqlDialect)) {
			
			return Collections.singletonList("ALTER TABLE IF EXISTS test_users " +
				"ADD COLUMN address IF NOT EXISTS VARCHAR(255) NULL");
		}
		
		return null;
	}
}
