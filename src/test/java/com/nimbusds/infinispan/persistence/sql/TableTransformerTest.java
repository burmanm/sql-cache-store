package com.nimbusds.infinispan.persistence.sql;


import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.jooq.impl.DSL.table;

import com.nimbusds.infinispan.persistence.sql.config.SQLStoreConfigurationBuilder;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.jooq.SQLDialect;
import org.junit.After;
import org.junit.Before;


/**
 * Tests the SQL store with a programmatic config and a table transformer.
 */
public class TableTransformerTest extends TestWithSQLDatabase {


	public static final String CACHE_NAME = "myMap";


	/**
	 * The Infinispan cache manager.
	 */
	protected EmbeddedCacheManager cacheMgr;
	
	
	private static Connection getSQLConnection()
		throws SQLException {
		
		return ((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getDataSource().getConnection();
	}


	@Before
	@Override
	public void setUp()
		throws Exception {
		
		setUp(SQLDialect.H2, new File("jdbc-h2.properties"));
	}


	public void setUp(final SQLDialect sqlDialect, final File testPropsFile)
		throws Exception {

		super.setUp(sqlDialect, testPropsFile);
		
		Properties props = getTestProperties(testPropsFile);

		cacheMgr = new DefaultCacheManager();

		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(SQLStoreConfigurationBuilder.class)
			.recordTransformerClass(UserRecordAndTableTransformer.class) // implements SQLTableTransformer
			.queryExecutorClass(UserQueryExecutor.class)
			.sqlDialect(sqlDialect)
			.createTableIfMissing(true) // optional, defaults to true
			.withProperties(props)
			.create();

		b.memory()
			.evictionStrategy(EvictionStrategy.REMOVE)
			.size(100)
			.evictionType(EvictionType.COUNT)
			.create();

		b.expiration()
			.wakeUpInterval(50L, TimeUnit.MILLISECONDS)
			.create();

		cacheMgr.defineConfiguration(CACHE_NAME, b.build());

		cacheMgr.start();
	}


	@After
	@Override
	public void tearDown()
		throws Exception {
		
		// Shut down Infinispan first
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		// Shut down DB
		super.tearDown();
	}
	
	
	public void testGetSQLRecordTransformer() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		SQLRecordTransformer<String, User> tr = ((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getSQLRecordTransformer();
		
		assertEquals(new UserRecordTransformer().getTableName(), tr.getTableName());
	}
	
	
	public void testTableColumnAdded() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertNotNull(SQLStore.getInstances().get(CACHE_NAME));
		assertEquals(1, SQLStore.getInstances().size());
		
		List<String> columnNames = SQLTableUtils.getColumnNames(table("test_users"), sql);
		assertEquals(Arrays.asList("uid", "given_name", "surname", "email", "address", "created", "permissions"), columnNames);
	}
}