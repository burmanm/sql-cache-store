package com.nimbusds.infinispan.persistence.sql;


import java.util.Collections;
import java.util.List;

import static org.jooq.impl.DSL.table;

import org.jooq.exception.DataAccessException;


public class SQLTableUtilsTest extends TestWithSQLDatabase {
	
	
	public void testInvalidTable(){
		
		try {
			SQLTableUtils.getColumnNames(table("no-such-table"), sql);
			fail();
		} catch (DataAccessException e) {
			assertTrue(e.getMessage().startsWith("SQL"));
		}
	}
	
	
	public void testGetColumnNames_zero() {
		
		sql.execute("CREATE TABLE zero_col_table");
		
		List<String> colsNames = SQLTableUtils.getColumnNames(table("zero_col_table"), sql);
		
		assertTrue(colsNames.isEmpty());
		
		sql.dropTable("zero_col_table");
	}
	
	
	public void testGetColumnNames_one() {
		
		sql.execute("CREATE TABLE one_col_table (name VARCHAR(100))");
		
		List<String> colsNames = SQLTableUtils.getColumnNames(table("one_col_table"), sql);
		
		assertEquals(Collections.singletonList("name"), colsNames);
		
		sql.dropTable("one_col_table");
	}
	
	
	public void testGetColumnNames_two() {
		
		sql.execute("CREATE TABLE two_col_table (name VARCHAR(100), email VARCHAR(100))");
		
		List<String> colsNames = SQLTableUtils.getColumnNames(table("two_col_table"), sql);
		
		assertEquals("name", colsNames.get(0));
		assertEquals("email", colsNames.get(1));
		assertEquals(2, colsNames.size());
		
		sql.dropTable("two_col_table");
	}
}
