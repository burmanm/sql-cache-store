package com.nimbusds.infinispan.persistence.sql;


import java.sql.Connection;
import java.sql.SQLException;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.spi.PersistenceException;
import org.jooq.Record;
import org.jooq.impl.DSL;


/**
 * Tests the SQL store with an XML-based config.
 */
public class SQLStoreWithXMLConfigSkipTableCreationOnStartupTest extends TestWithSQLDatabase {


	private static final String CACHE_NAME = "userMap";


	/**
	 * The Infinispan cache manager.
	 */
	protected EmbeddedCacheManager cacheMgr;
	
	
	private static Connection getSQLConnection()
		throws SQLException {
		
		return ((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getDataSource().getConnection();
	}


	@Override
	public void setUp()
		throws Exception {

		super.setUp();

		cacheMgr = new DefaultCacheManager("test-infinispan-alt.xml");

		cacheMgr.start();
	}


	@Override
	public void tearDown()
		throws Exception {
		
		// Shut down Infinispan first
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		// Shut down H2 DB
		super.tearDown();
	}
	
	
	public void testTableNotCreated() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		try {
			myMap.put("alice", new User("alice", "alice@wonderland.net"));
			fail();
		} catch (PersistenceException e) {
			assertTrue(e.getMessage().contains("Table \"test_users\" not found"));
		}
	}


	public void testSimpleObjectLifeCycle()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		// Create the database before the cache gets used
		UserRecordTransformer t = new UserRecordTransformer();
		t.init(()->sqlDialect);
		sql.execute(t.getCreateTableStatement());
		
		assertNotNull(SQLStore.getInstances().get(CACHE_NAME));
		assertEquals(1, SQLStore.getInstances().size());
		
		// Initial size
		assertEquals(0, myMap.size());
		
		// Get non-existing object
		assertNull(myMap.get("invalid-key"));
		
		// Store new object
		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@wonderland.net");
		
		assertNull(myMap.putIfAbsent(k1, u1));
		
		// Check presence
		assertTrue(myMap.containsKey(k1));
		
		
		// Confirm SQL insert
		Record r = sql.select().from(table("test_users")).where(field("uid").eq(k1)).fetchOne();
		assertEquals(k1, r.get("uid"));
		assertEquals("Alice",  r.get("given_name"));
		assertEquals("Adams",  r.get("surname"));
		assertEquals("alice@wonderland.net",  r.get("email"));
		assertNull(r.get("created"));
		assertNull(r.get("permissions"));
		assertEquals(6, r.size());
		
		
		// Get new count
		assertEquals(1, myMap.size());
		
		// Get object
		assertEquals("Alice Adams", myMap.get(k1).getName());
		assertEquals("alice@wonderland.net", myMap.get(k1).getEmail());
		
		// Update object
		u1 = new User("Bob Brown", "bob@wonderland.net");
		assertNotNull(myMap.replace(k1, u1));
		
		// Confirm SQL update
		r = sql.select().from(table("test_users")).where(field("uid").eq(k1)).fetchOne();
		assertEquals(k1, r.get("uid"));
		assertEquals("Bob",  r.get("given_name"));
		assertEquals("Brown",  r.get("surname"));
		assertEquals("bob@wonderland.net",  r.get("email"));
		assertNull(r.get("created"));
		assertNull(r.get("permissions"));
		assertEquals(6, r.size());
		
		// Get object
		assertEquals("Bob Brown", myMap.get(k1).getName());
		assertEquals("bob@wonderland.net", myMap.get(k1).getEmail());
		
		// Remove object
		u1 = myMap.remove(k1);
		assertEquals("Bob Brown", u1.getName());
		assertEquals("bob@wonderland.net", u1.getEmail());
		
		// Confirm removal
		assertNull(myMap.get(k1));
		assertFalse(myMap.containsKey(k1));
		
		// Confirm SQL delete
		try (Connection c = getSQLConnection()) {
			assertNull(DSL.using(c).select().from(table("test_users")).where(field("uid").eq(k1)).fetchOne());
		}
		
		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
	}
}