package com.nimbusds.infinispan.persistence.sql;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import junit.framework.TestCase;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;


/**
 * The base class for tests that require an SQL database.
 */
public abstract class TestWithSQLDatabase extends TestCase {
	
	
	/**
	 * The SQL dialect.
	 */
	protected SQLDialect sqlDialect;
	
	
	/**
	 * Keep this SQL connection for the duration of the test. Required by
	 * the H2 test to prevent the in memory DB from stopping.
	 */
	protected Connection persistentConnection;
	
	
	/**
	 * DSL context wrapping the {@link #persistentConnection}.
	 */
	protected DSLContext sql;
	

	/**
	 * Returns the test configuration properties.
	 *
	 * @param testPropsFile The DB-specified test properties file.
	 *
	 * @return The test configuration properties.
	 */
	public static Properties getTestProperties(final File testPropsFile) {

		Properties props = new Properties();

		try {
			props.load(new FileInputStream(testPropsFile));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return props;
	}
	
	
	/**
	 * Defaults to in-memory H2 database.
	 */
	public void setUp()
		throws Exception {
		
		setUp(SQLDialect.H2, new File("jdbc-h2.properties"));
	}
	
	
	public void setUp(final SQLDialect sqlDialect, final File testPropsFile)
		throws Exception {
		
		this.sqlDialect = sqlDialect;
		
		Properties props = getTestProperties(testPropsFile);
		
		String userName = props.getProperty("username");
		String password = props.getProperty("password");
		String url = props.getProperty("jdbcUrl");
		
		// Connection is the only JDBC resource that we need
		// PreparedStatement and ResultSet are handled by jOOQ, internally
		persistentConnection = DriverManager.getConnection(url, userName, password);
		
		System.out.println("Setting up " + sqlDialect + " test database " + url);
		
		sql = DSL.using(persistentConnection, sqlDialect);
		
		String tableName = new UserRecordTransformer().getTableName();
		
		if (sql.dropTableIfExists(tableName).execute() > 0) {
			System.out.println("Dropped " + tableName + " table at setup");
		}
	}


	@Override
	public void tearDown()
		throws Exception {
		
		if (sql == null) {
			return;
		}
		
		if (sql.dropTableIfExists(new UserRecordTransformer().getTableName()).execute() > 0) {
			System.out.println("Dropped " + new UserRecordTransformer().getTableName() + " table at tear down");
		}
		
		sql.close();
	}
	
	
	public static String toSQLTimestamp(final Instant instant) {
		
		// Timestamp in local timezone
		OffsetDateTime dt = OffsetDateTime.ofInstant(instant, ZoneId.systemDefault());
		return DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss.S").format(dt);
	}
}
