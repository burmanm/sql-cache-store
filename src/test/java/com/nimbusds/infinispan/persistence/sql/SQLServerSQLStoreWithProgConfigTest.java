package com.nimbusds.infinispan.persistence.sql;


import java.io.File;
import java.util.Properties;

import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.jooq.SQLDialect;
import org.junit.Before;

import com.nimbusds.infinispan.persistence.sql.config.SQLStoreConfigurationBuilder;
import com.nimbusds.infinispan.persistence.sql.transformers.SQLFieldTransformer;


public class SQLServerSQLStoreWithProgConfigTest extends SimpleObjectLifeCycleTest {
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {
		
		File configFile = new File("jdbc-sqlserver.properties");
		
		super.setUp(SQLDialect.SQLSERVER2016, configFile);
		
		Properties props = getTestProperties(configFile);
		
		cacheMgr = new DefaultCacheManager();
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(SQLStoreConfigurationBuilder.class)
			.recordTransformerClass(UserRecordTransformer.class)
			.sqlDialect(SQLDialect.SQLSERVER2016)
			.withProperties(props)
			.create();
		
		b.memory()
			.evictionStrategy(EvictionStrategy.REMOVE)
			.size(100)
			.evictionType(EvictionType.COUNT)
			.create();
		
		cacheMgr.defineConfiguration(CACHE_NAME, b.build());
		
		cacheMgr.start();
		
		sqlFieldTransformer = new SQLFieldTransformer(SQLDialect.SQLSERVER2016);
	}
}
