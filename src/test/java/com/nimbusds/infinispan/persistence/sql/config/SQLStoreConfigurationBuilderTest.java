package com.nimbusds.infinispan.persistence.sql.config;


import static org.junit.Assert.*;

import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.cache.PersistenceConfigurationBuilder;
import org.junit.Test;


public class SQLStoreConfigurationBuilderTest {
	

	@Test
	public void testConstructor() {
		
		PersistenceConfigurationBuilder persistenceConfigurationBuilder = new ConfigurationBuilder().persistence();
		
		SQLStoreConfigurationBuilder b = new SQLStoreConfigurationBuilder(persistenceConfigurationBuilder);
		
		SQLStoreConfiguration config = b.create();
		
		// Infinispan
		assertFalse(config.purgeOnStartup());
		assertFalse(config.fetchPersistentState());
		assertFalse(config.ignoreModifications());
		assertNotNull(config.async());
		assertFalse(config.async().enabled());
		assertNotNull(config.singletonStore());
		assertFalse(config.singletonStore().enabled());
		assertFalse(config.preload());
		assertFalse(config.shared());
		assertTrue(config.createTableIfMissing());
		assertNull(config.getConnectionPool());
	}
}
