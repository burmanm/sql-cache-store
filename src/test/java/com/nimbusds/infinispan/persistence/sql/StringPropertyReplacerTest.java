package com.nimbusds.infinispan.persistence.sql;


import java.util.Properties;
import java.util.UUID;

import junit.framework.TestCase;
import org.infinispan.commons.util.StringPropertyReplacer;


public class StringPropertyReplacerTest extends TestCase {
	
	
	public void testNoReplace() {
		
		Properties props = new Properties();
		props.setProperty("sqlStore.sqlDialect", "H2");
		String out = StringPropertyReplacer.replaceProperties("MySQL", props);
		assertEquals("MySQL", out);
	}
	
	
	public void testReplace() {
		
		Properties props = new Properties();
		props.setProperty("sqlStore.sqlDialect", "H2");
		String out = StringPropertyReplacer.replaceProperties("${sqlStore.sqlDialect}", props);
		assertEquals("H2", out);
	}
	
	
	public void testReplaceUsingSystemProperties() {
		
		String randomName = UUID.randomUUID().toString();
		
		System.getProperties().setProperty(randomName, "abc");
		
		String out = StringPropertyReplacer.replaceProperties("${" + randomName + "}");
		assertEquals("abc", out);
		
		System.getProperties().remove(randomName);
	}
	
	
	public void testReplaceDefault() {
		
		Properties props = new Properties();
		String out = StringPropertyReplacer.replaceProperties("${sqlStore.sqlDialect:H2}", props);
		assertEquals("H2", out);
	}
}
