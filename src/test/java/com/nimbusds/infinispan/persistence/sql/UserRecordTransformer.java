package com.nimbusds.infinispan.persistence.sql;


import java.sql.Timestamp;
import java.util.*;

import static org.jooq.impl.DSL.field;

import org.jooq.*;
import org.jooq.impl.SQLDataType;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.sql.transformers.CustomSQLDataTypes;
import com.nimbusds.infinispan.persistence.sql.transformers.SQLFieldTransformer;
import com.nimbusds.infinispan.persistence.sql.transformers.SQLServerNVarcharBinding;


/**
 * Transforms User POJO to / from SQL record.
 */
public class UserRecordTransformer implements SQLRecordTransformer<String,User> {
	
	
	protected SQLDialect sqlDialect;
	
	
	private SQLFieldTransformer sqlFieldTransformer;
	
	
	@Override
	public void init(final InitParameters initParams) {
		sqlDialect = initParams.sqlDialect();
		sqlFieldTransformer = new SQLFieldTransformer(sqlDialect);
	}
	
	
	@Override
	public String getCreateTableStatement() {
		
		if (SQLDialect.H2.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS test_users (" +
				"uid VARCHAR(255) PRIMARY KEY NOT NULL," +
				"given_name VARCHAR(255) NULL," +
				"surname VARCHAR(255) NULL," +
				"email VARCHAR(255) NULL," +
				"created TIMESTAMP NULL," +
				"permissions ARRAY NULL" +
				")";
		} 
		
		if (SQLDialect.MYSQL.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS test_users (" +
				"uid VARCHAR(255) NOT NULL," +
				"given_name VARCHAR(255) NULL," +
				"surname VARCHAR(255) NULL," +
				"email VARCHAR(255) NULL," +
				"created TIMESTAMP NULL," +
				"permissions JSON NULL," +
				"PRIMARY KEY (uid)" +
				") CHARACTER SET = utf8";
		}
		
		if (SQLDialect.POSTGRES_9_5.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS test_users (" +
				"uid VARCHAR(255) NOT NULL," +
				"given_name VARCHAR(255) NULL," +
				"surname VARCHAR(255) NULL," +
				"email VARCHAR(255) NULL," +
				"created TIMESTAMPTZ NULL," +
				"permissions TEXT[] NULL," +
				"CONSTRAINT pk PRIMARY KEY (uid)" +
				")";
		}
		
		if (SQLDialect.SQLSERVER2016.equals(sqlDialect)) {
			
			return "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='test_users' and xtype='U') " +
				"CREATE TABLE test_users (" +
				"uid VARCHAR(255) NOT NULL," +
				"given_name NVARCHAR(255) NULL," +
				"surname NVARCHAR(255) NULL," +
				"email VARCHAR(255) NULL," +
				"created DATETIME NULL," +
				"permissions NVARCHAR(4000) NULL," +
				"CONSTRAINT pk PRIMARY KEY (uid)" +
				")";
		}
		
		throw new UnsupportedOperationException("Unsupported SQL dialect: " + sqlDialect);
	}
	
	
	@Override
	public String getTableName() {
		
		return "test_users";
	}
	
	
	@Override
	public Collection<Condition> resolveSelectionConditions(final String key) {
		
		return Collections.singleton(field("uid").equal(key));
	}
	
	
	@Override
	public SQLRecord toSQLRecord(final InfinispanEntry<String,User> infinispanEntry) {
		
		Map<Field<?>,Object> fields = new LinkedHashMap<>();
		fields.put(field("uid"), infinispanEntry.getKey());
		
		String givenNameString = infinispanEntry.getValue().getName().split("\\s")[0];
		if (SQLDialect.SQLSERVER.family().equals(sqlDialect.family())) {
			fields.put(field("given_name", CustomSQLDataTypes.SQLSERVER_NVARCHAR), givenNameString);
		} else {
			fields.put(field("given_name"), givenNameString);
		}
		
		String surnameString = infinispanEntry.getValue().getName().split("\\s")[1];
		if (SQLDialect.SQLSERVER.family().equals(sqlDialect.family())) {
			fields.put(field("surname", CustomSQLDataTypes.SQLSERVER_NVARCHAR), surnameString);
		} else {
			fields.put(field("surname"), surnameString);
		}
		
		fields.put(field("email"), infinispanEntry.getValue().getEmail());
		
		if (infinispanEntry.getValue().getCreated() != null) {
			fields.put(field("created"), new Timestamp(infinispanEntry.getValue().getCreated().toEpochMilli()));
		} else {
			fields.put(field("created"), null);
		}
		
		if (infinispanEntry.getValue().getPermissions() != null) {
			
			Object value = sqlFieldTransformer.toSQLCollection(infinispanEntry.getValue().getPermissions());
			
			fields.put(field("permissions"), value);
			
		} else {
			fields.put(field("permissions"), null);
		}
		
		return new ImmutableSQLRecord("uid", fields);
	}
	
	
	@Override
	public InfinispanEntry<String,User> toInfinispanEntry(final Record sqlRecord) {
		
		String uid = sqlRecord.get(field("uid"), String.class);
		String givenName = sqlRecord.get(field("given_name"), String.class);
		String surname = sqlRecord.get(field("surname"), String.class);
		String name = givenName + " " + surname;
		String email = sqlRecord.get(field("email"), String.class);
		
		Timestamp created = null;
		if (sqlRecord.get(field("created")) != null) {
			created = sqlRecord.get(field("created"), Timestamp.class);
		}
		
		List<String> permissions = null;
		if (sqlRecord.get(field("permissions")) != null) {
			String[] permissionsArray = sqlFieldTransformer.parseSQLStringCollection("permissions", sqlRecord);
			if (permissionsArray != null) {
				permissions = Arrays.asList(permissionsArray);
			}
		}
		
		return new InfinispanEntry<>(
			uid,
			new User(
				name,
				email,
				created != null ? created.toInstant() : null,
				permissions),
			null);
	}
}
