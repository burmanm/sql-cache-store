package com.nimbusds.infinispan.persistence.sql.transformers;


import java.net.URI;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;

import junit.framework.TestCase;
import net.minidev.json.JSONObject;
import org.infinispan.persistence.spi.PersistenceException;
import org.jooq.SQLDialect;

import com.nimbusds.infinispan.persistence.sql.TestWithSQLDatabase;


public class SQLFieldTransformerTest extends TestCase {
	
	
	// Transformer helpers
	public void testObjectToString() {
		
		assertEquals("abc", SQLFieldTransformer.toString("abc"));
		
		assertEquals("ARRAY", SQLFieldTransformer.toString(CollectionDataType.ARRAY));
		
		assertEquals("https://c2id.com", SQLFieldTransformer.toString(URI.create("https://c2id.com")));
		
		assertNull(SQLFieldTransformer.toString(null));
	}
	
	
	public void testToSQLCollection_asJSONArray_mySQL() {
		
		// No SQL array support, serialise to JSON array
		SQLFieldTransformer t = new SQLFieldTransformer(SQLDialect.MYSQL);
		
		assertEquals("[\"urn:a:b\",\"urn:c:d\"]", t.toSQLCollection(Arrays.asList(URI.create("urn:a:b"), URI.create("urn:c:d"))));
		
		assertNull(t.toSQLCollection(null));
	}
	
	
	public void testToSQLCollection_asJSONArray_sqlServer() {
		
		// No SQL array support, serialise to JSON array
		SQLFieldTransformer t = new SQLFieldTransformer(SQLDialect.SQLSERVER2016);
		
		assertEquals("[\"urn:a:b\",\"urn:c:d\"]", t.toSQLCollection(Arrays.asList(URI.create("urn:a:b"), URI.create("urn:c:d"))));
		
		assertNull(t.toSQLCollection(null));
	}
	
	
	public void testToSQLCollection_asStringArray() {
		
		// Has SQL array support
		for (SQLDialect sqlDialect: Arrays.asList(SQLDialect.H2, SQLDialect.POSTGRES_11, SQLDialect.POSTGRES_10, SQLDialect.POSTGRES_9_5, SQLDialect.POSTGRES_9_4, SQLDialect.POSTGRES_9_3)) {
			
			SQLFieldTransformer t = new SQLFieldTransformer(sqlDialect);
			
			assertTrue(Arrays.equals(new String[]{"urn:a:b", "urn:c:d"}, (String[]) t.toSQLCollection(Arrays.asList(URI.create("urn:a:b"), URI.create("urn:c:d")))));
			
			assertNull(t.toSQLCollection(null));
		}
	}
	
	
	public void testToSQLString_jsonObject() {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("a", 123);
		assertEquals("{\"a\":123}", SQLFieldTransformer.toSQLString(jsonObject));
		
		assertNull(SQLFieldTransformer.toSQLString(null));
	}
	
	
	public void testToSQLTimestamp() {
		
		assertEquals(TestWithSQLDatabase.toSQLTimestamp(Instant.EPOCH), SQLFieldTransformer.toTimestamp(Instant.EPOCH).toString());
		
		Instant instant = Instant.ofEpochSecond(1473946131L);
		
		assertEquals(TestWithSQLDatabase.toSQLTimestamp(instant), SQLFieldTransformer.toTimestamp(instant).toString());
		
		assertNull(SQLFieldTransformer.toTimestamp(null));
	}
	
	
	// Parse helpers
	public void testParseJSONObject() {
		
		JSONObject jsonObject = SQLFieldTransformer.parseJSONObject("{\"ip\":\"192.168.0.1\"}");
		assertEquals("192.168.0.1", jsonObject.get("ip"));
		assertEquals(1, jsonObject.size());
		
		assertNull(SQLFieldTransformer.parseJSONObject(null));
		
		try {
			SQLFieldTransformer.parseJSONObject("[]");
			fail();
		} catch (PersistenceException e) {
			assertTrue(e.getMessage().startsWith("Couldn't parse JSON object: "));
		}
	}
	
	
	public void testParseStringArrayFromJSONArrayString_twoElements() {
	
		String s = "[\"openid\",\"email\"]";
		
		String[] values = SQLFieldTransformer.parseStringArrayFromJSONArrayString(s);
		
		assertEquals("openid", values[0]);
		assertEquals("email", values[1]);
		assertEquals(2, values.length);
	}
	
	
	public void testParseStringArrayFromJSONArrayString_empty() {
	
		String s = "[]";
		
		String[] values = SQLFieldTransformer.parseStringArrayFromJSONArrayString(s);
		assertEquals(0, values.length);
	}
	
	
	public void testParseStringArrayFromJSONArrayString_null() {
	
		assertEquals(null, SQLFieldTransformer.parseStringArrayFromJSONArrayString(null));
	}
	
	
	// https://bitbucket.org/connect2id/server/issues/470/corrupted-scp-and-clm-in-authz-record-in
	public void testParseStringArrayFromJSONArrayString_arrayOfJSONObjects() {
	
		String corruptValue = "[{}, {}, {}, {}, {}, {}, {}]";
		
		try {
			SQLFieldTransformer.parseStringArrayFromJSONArrayString(corruptValue);
			fail();
		} catch (PersistenceException e) {
			assertTrue(e.getMessage().startsWith("Couldn't parse JSON array of strings: "));
			assertTrue(e.getCause() instanceof ArrayStoreException);
		}
	}
	
	
	public void testParseDate() {
		
		Instant now = Instant.ofEpochMilli(Instant.now().toEpochMilli());
		
		assertEquals(now, SQLFieldTransformer.parseInstant(new Timestamp(now.toEpochMilli())));
		
		assertNull(SQLFieldTransformer.parseInstant(null));
	}
}
