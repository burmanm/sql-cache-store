package com.nimbusds.infinispan.persistence.sql;


import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;
import static org.junit.Assert.assertArrayEquals;

import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;
import org.jooq.Record;
import org.jooq.impl.DSL;
import org.junit.After;

import com.nimbusds.common.monitor.MonitorRegistries;
import com.nimbusds.infinispan.persistence.sql.transformers.SQLFieldTransformer;


public class SimpleObjectLifeCycleTest extends TestWithSQLDatabase {
	
	
	public static final String CACHE_NAME = "userMap";
	
	
	/**
	 * The Infinispan cache manager.
	 */
	protected EmbeddedCacheManager cacheMgr;
	
	
	protected SQLFieldTransformer sqlFieldTransformer;
	
	
	private static Connection getSQLConnection()
		throws SQLException {
		
		return ((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getDataSource().getConnection();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		// Shut down Infinispan first
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		// Shut down DB
		super.tearDown();
	}
	
	
	public void testSimpleObjectLifeCycle()
		throws Exception {
		
		if (cacheMgr == null) {
			System.out.println("Skipping base test, must be run from inheriting class");
			return;
		}
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertNotNull(SQLStore.getInstances().get(CACHE_NAME));
		assertEquals(1, SQLStore.getInstances().size());
		
		// Initial size
		assertEquals(0, myMap.size());
		
		// Get non-existing object
		assertNull(myMap.get("invalid-key"));
		
		// Store new object with all fields defined
		Instant created = Instant.ofEpochMilli(1474012953000L);
		User u1 = new User("Alice Adams", "alice@wonderland.net", created, Arrays.asList("admin", "audit"));
		assertNull(myMap.putIfAbsent("u1", u1));
		
		// Check presence
		assertTrue(myMap.containsKey("u1"));
		
		
		// Confirm SQL insert
		Record r = sql.select().from(table("test_users")).where(field("uid").eq("u1")).fetchOne();
		assertEquals("u1", r.get("uid"));
		assertEquals("Alice",  r.get("given_name"));
		assertEquals("Adams",  r.get("surname"));
		assertEquals("alice@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant()); // Timestamp
		assertArrayEquals(new String[]{"admin", "audit"}, sqlFieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());
		
		
		// Get new count
		assertEquals(1, myMap.size());
		
		// Get object
		User out = myMap.get("u1");
		assertEquals(u1.getName(), out.getName());
		assertEquals(u1.getEmail(), out.getEmail());
		assertEquals(u1.getCreated(), out.getCreated());
		assertEquals(u1.getPermissions(), out.getPermissions());
		
		// Update object
		User u2 = new User("Bob Brown", "bob@wonderland.net", created, Arrays.asList("browse", "pay"));
		assertNotNull(myMap.replace("u1", u2));
		
		// Confirm SQL update
		r = sql.select().from(table("test_users")).where(field("uid").eq("u1")).fetchOne();
		assertEquals("u1", r.get("uid"));
		assertEquals("Bob",  r.get("given_name"));
		assertEquals("Brown",  r.get("surname"));
		assertEquals("bob@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant()); // Timestamp
		assertArrayEquals(new String[]{"browse", "pay"}, sqlFieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());
		
		// Get object
		out = myMap.get("u1");
		assertEquals(u2.getName(), out.getName());
		assertEquals(u2.getEmail(), out.getEmail());
		assertEquals(u2.getCreated(), out.getCreated());
		assertEquals(u2.getPermissions(), out.getPermissions());
		
		// Update object with fewer defined fields
		User u3 = new User("Claire Cox", "claire@wonderland.net");
		assertNotNull(myMap.replace("u1", u3));
		
		// Confirm SQL update
		r = sql.select().from(table("test_users")).where(field("uid").eq("u1")).fetchOne();
		assertEquals("u1", r.get("uid"));
		assertEquals("Claire",  r.get("given_name"));
		assertEquals("Cox",  r.get("surname"));
		assertEquals("claire@wonderland.net",  r.get("email"));
		assertNull(r.get("created")); // Timestamp
		assertNull(r.get("permissions"));
		assertEquals(6, r.size());
		
		// Get object
		out = myMap.get("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		
		// Remove object
		out = myMap.remove("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		// Confirm removal
		assertNull(myMap.get("u1"));
		assertFalse(myMap.containsKey("u1"));
		
		// Confirm SQL delete
		try (Connection c = getSQLConnection()) {
			assertNull(DSL.using(c).select().from(table("test_users")).where(field("uid").eq("u1")).fetchOne());
		}
		
		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
		
		MonitorRegistries.getMetricRegistry().getTimers().keySet().forEach(System.out::println);
		MonitorRegistries.getMetricRegistry().getGauges().keySet().forEach(System.out::println);
	}
	
	
	public void testI18N()
		throws Exception {
		
		if (cacheMgr == null) {
			System.out.println("Skipping base test, must be run from inheriting class");
			return;
		}
		
		System.out.println("Default JVM encoding: " + Charset.defaultCharset());
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		// Store new object with all fields defined
		Instant created = Instant.ofEpochMilli(1474012953000L);
		User u1 = new User("Алиса Адамс", "alice@wonderland.net", created, Arrays.asList("admin", "audit"));
		assertNull(myMap.putIfAbsent("u1", u1));
		
		// Check presence
		assertTrue(myMap.containsKey("u1"));
		
		
		// Confirm SQL insert
		Record r = sql.select().from(table("test_users")).where(field("uid").eq("u1")).fetchOne();
		System.out.println(r);
		assertEquals("u1", r.get("uid").toString());
		assertEquals("Алиса",  r.get("given_name"));
		assertEquals("Адамс",  r.get("surname"));
		assertEquals("alice@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant()); // Timestamp
		assertArrayEquals(new String[]{"admin", "audit"}, sqlFieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());
		
		// Get object
		myMap.getAdvancedCache().getDataContainer().clear();
		User out = myMap.get("u1");
		assertEquals(u1.getName(), out.getName());
		assertEquals(u1.getEmail(), out.getEmail());
		assertEquals(u1.getCreated(), out.getCreated());
		assertEquals(u1.getPermissions(), out.getPermissions());
		
		// Remove object
		out = myMap.remove("u1");
		assertNotNull(out);
		
		// Confirm removal
		assertNull(myMap.get("u1"));
		assertFalse(myMap.containsKey("u1"));
		
		// Confirm SQL delete
		try (Connection c = getSQLConnection()) {
			assertNull(DSL.using(c).select().from(table("test_users")).where(field("uid").eq("u1")).fetchOne());
		}
		
		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
	}
}
