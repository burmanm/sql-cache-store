package com.nimbusds.infinispan.persistence.sql;


import java.io.File;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.jooq.impl.DSL.table;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import com.nimbusds.infinispan.persistence.sql.config.SQLStoreConfiguration;
import com.nimbusds.infinispan.persistence.sql.config.SQLStoreConfigurationBuilder;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.marshall.core.MarshalledEntry;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.junit.Before;


public class ExpiredEntryReaperTest extends TestWithSQLDatabase {
	
	
	public static class ExpiringUserRecordTransformer extends UserRecordTransformer {
		
		
		@Override
		public InfinispanEntry<String,User> toInfinispanEntry(final Record sqlRecord) {
		
			InfinispanEntry<String,User> baseEntry = super.toInfinispanEntry(sqlRecord);
			
			final Date now = new Date();
			final Date yesterday = new Date(now.getTime() - 24*60*60*1000);
			
			// Simulate expired entry
			InternalMetadata metadata = new InternalMetadataBuilder()
				.created(yesterday)
				.lastUsed(yesterday)
				.lifespan(6L, TimeUnit.HOURS)
				.maxIdle(1L, TimeUnit.HOURS)
				.build();
			
			assertTrue(metadata.isExpired(now.getTime()));
			
			return new InfinispanEntry<>(baseEntry.getKey(), baseEntry.getValue(), metadata);
		}
	}
	
	
	DSLContext dsl;
	
	
	SQLRecordTransformer<String,User> entityTransformer;
	
	
	@Before
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		SQLStoreConfiguration config = new SQLStoreConfigurationBuilder(new ConfigurationBuilder().persistence())
			.recordTransformerClass(UserRecordTransformer.class)
			.sqlDialect(SQLDialect.H2)
			.properties(getTestProperties(new File("jdbc-h2.properties")))
			.create();
		
		entityTransformer = new ExpiringUserRecordTransformer();
		entityTransformer.init(() -> SQLDialect.H2);
		
		dsl = DSL.using(persistentConnection, config.getSQLDialect());
		
		// Create table
		dsl.execute(entityTransformer.getCreateTableStatement());
		
		for (int i=0; i < 2000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			int inserted = dsl.insertInto(table(entityTransformer.getTableName()))
				.set(entityTransformer.toSQLRecord(new InfinispanEntry<>(i + "", user, null)).getFields())
				.execute();
			
			assertEquals(1, inserted);
		}
		
		assertEquals(2000, dsl.fetchCount(table(entityTransformer.getTableName())));
	}
	
	
	public void testExpireWithKeyListener() {
		
		ExpiredEntryReaper<String,User> reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), dsl, entityTransformer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		reaper.purge(
			key -> {
				assertNotNull(key);
				deletedKeys.add(key);
			});
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}
	
	
	public void testExpireWithEntryListener() {
		
		ExpiredEntryReaper<String,User> reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), dsl, entityTransformer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		reaper.purgeExtended(
			new AdvancedCacheExpirationWriter.ExpirationPurgeListener<String, User>() {
				@Override
				public void marshalledEntryPurged(MarshalledEntry<String, User> marshalledEntry) {
					deletedKeys.add(marshalledEntry.getKey());
					assertNotNull(marshalledEntry.getValue());
					assertNotNull(marshalledEntry.getMetadata());
				}
				
				@Override
				public void entryPurged(String key) {
					// must not be called
					assert false;
				}
			});
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}
}
