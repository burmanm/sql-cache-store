package com.nimbusds.infinispan.persistence.sql;


import junit.framework.TestCase;


public class JOOQFixesTest extends TestCase {
	

	public void testExtractPrimaryKeyConstraintName() {
		
		String stmt =
			"CREATE TABLE IF NOT EXISTS test_users (" +
			"uid VARCHAR(255) NOT NULL," +
			"given_name VARCHAR(255) NULL," +
			"surname VARCHAR(255) NULL," +
			"email VARCHAR(255) NULL," +
			"created TIMESTAMP NULL," +
			"permissions TEXT[] NULL," +
			"CONSTRAINT pk PRIMARY KEY (uid)" +
			")";
		
		assertEquals("pk", JOOQFixes.parsePrimaryKeyConstraintName(stmt));
	}
	

	public void testExtractPrimaryKeyConstraintName_lowerCase() {
		
		String stmt =
			"CREATE TABLE IF NOT EXISTS test_users (" +
			"uid VARCHAR(255) NOT NULL," +
			"given_name VARCHAR(255) NULL," +
			"surname VARCHAR(255) NULL," +
			"email VARCHAR(255) NULL," +
			"created TIMESTAMP NULL," +
			"permissions TEXT[] NULL," +
			"CONSTRAINT pk PRIMARY KEY (uid)" +
			")";
		
		assertEquals("pk", JOOQFixes.parsePrimaryKeyConstraintName(stmt.toLowerCase()));
	}
	

	public void testExtractPrimaryKeyConstraintName_extraWhiteSpace() {
		
		String stmt =
			"CREATE TABLE IF NOT EXISTS test_users (" +
			"uid VARCHAR(255) NOT NULL," +
			"given_name VARCHAR(255) NULL," +
			"surname VARCHAR(255) NULL," +
			"email VARCHAR(255) NULL," +
			"created TIMESTAMP NULL," +
			"permissions TEXT[] NULL," +
			"CONSTRAINT    pk    PRIMARY KEY (uid)" +
			")";
		
		assertEquals("pk", JOOQFixes.parsePrimaryKeyConstraintName(stmt.toLowerCase()));
	}
}
