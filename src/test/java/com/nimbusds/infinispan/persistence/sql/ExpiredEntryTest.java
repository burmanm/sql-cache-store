package com.nimbusds.infinispan.persistence.sql;


import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.table;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import com.nimbusds.infinispan.persistence.sql.config.SQLStoreConfigurationBuilder;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.junit.After;
import org.junit.Before;


public class ExpiredEntryTest extends TestWithSQLDatabase {
	
	
	public static final String CACHE_NAME = "userMap";
	
	
	/**
	 * The Infinispan cache manager.
	 */
	protected EmbeddedCacheManager cacheMgr;
	
	
	private Cache<String,User> myMap;
	
	
	private DSLContext dsl;
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		setUp(SQLDialect.H2, new File("jdbc-h2.properties"));
		
		myMap = cacheMgr.getCache(CACHE_NAME);
		
		dsl = DSL.using(((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getDataSource().getConnection(), sqlDialect, null);
	}
	
	
	public void setUp(final SQLDialect sqlDialect, final File testPropsFile)
		throws Exception {
		
		super.setUp(sqlDialect, testPropsFile);
		
		Properties props = getTestProperties(testPropsFile);
		
		cacheMgr = new DefaultCacheManager();
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(SQLStoreConfigurationBuilder.class)
			.recordTransformerClass(ExpiredEntryReaperTest.ExpiringUserRecordTransformer.class)
			.queryExecutorClass(UserQueryExecutor.class)
			.sqlDialect(sqlDialect)
			.createTableIfMissing(true) // optional, defaults to true
			.withProperties(props)
			.create();
		
		b.memory()
			.evictionStrategy(EvictionStrategy.REMOVE)
			.size(100)
			.evictionType(EvictionType.COUNT)
			.create();
		
		b.expiration()
			.wakeUpInterval(1L, TimeUnit.MINUTES)
			.create();
		
		cacheMgr.defineConfiguration(CACHE_NAME, b.build());
		
		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		// Shut down Infinispan first
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		// Shut down DB
		super.tearDown();
	}
	
	
	private void storeExpiredEntry(final String key) {
		
		User user = new User(
			"Alice Adams",
			"alice@wonderland.net",
			Instant.now().minus(1L, ChronoUnit.DAYS),
			Collections.singletonList("admin"));
		
		UserRecordTransformer t = new ExpiredEntryReaperTest.ExpiringUserRecordTransformer();
		t.init(() -> sqlDialect);
		
		SQLRecord sqlRecord = t.toSQLRecord(new InfinispanEntry<>(
			key,
			user,
			new InternalMetadataBuilder()
				.created(Date.from(Instant.now().minus(1L, ChronoUnit.DAYS)))
				.lifespan(1L, TimeUnit.HOURS)
				.build())
		);
		
		dsl.mergeInto(table(t.getTableName()), sqlRecord.getFields().keySet())
			.key(sqlRecord.getKeyColumns())
			.values(sqlRecord.getFields().values())
			.execute();
	}
	
	
	public void testDontReturnExpiredEntry() {
		
		String key = "u1";
		storeExpiredEntry(key);
		
		assertEquals(1, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
		
		assertNull(myMap.get(key));
	}
	
	
	public void testDontProcessExpiredEntry() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String key = "u1";
		storeExpiredEntry(key);
		
		assertEquals(1, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
		
		Map<String,User> filteredMap = myMap
			.entrySet()
			.stream()
			.filter(uuidUserEntry -> true)
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertTrue(filteredMap.isEmpty());
	}
	
	
	public void testPurge()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String key = "u1";
		storeExpiredEntry(key);
		
		assertEquals(1, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
		
		myMap.getAdvancedCache().getExpirationManager().processExpiration();
		
		Thread.sleep(100);
		
		assertEquals(0, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
	}
}
